<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Articles

Route::get('/articles', function() {

    // just simple hardcoded articles for now
    // this needs a model to get articles from the db

    return '{"articles":[
        {
            "id": 1,
            "title":"Control",
            "text":"After a secretive agency in New York is invaded by an otherworldly threat, you become the new Director struggling to regain Control.",
            "image": "https://upload.wikimedia.org/wikipedia/en/6/6a/D1IOd0BWsAAiX5T.jpg"
        },
        {
            "id": 2,
            "title":"The Outer Worlds",
            "text":"The Outer Worlds is a new single-player first-person sci-fi RPG from Obsidian Entertainment and Private Division.",
            "image": "https://upload.wikimedia.org/wikipedia/en/e/e7/The_Outer_Worlds_cover_art.png"
        },
        {
            "id": 3,
            "title":"Red Dead Redemption 2",
            "text":"Red Dead Redemption 2, the critically acclaimed open world epic from Rockstar Games and the highest rated game of the console generation, now enhanced for PC with new Story Mode content, visual upgrades and more.",
            "image": "https://upload.wikimedia.org/wikipedia/en/4/44/Red_Dead_Redemption_II.jpg"
        },
        {
            "id": 4,
            "title":"STAR WARS Jedi: Fallen Order",
            "text":"A galaxy-spanning adventure awaits in STAR WARS Jedi: Fallen Order, a new 3rd person action-adventure title from Respawn Entertainment.",
            "image": "https://upload.wikimedia.org/wikipedia/en/1/13/Cover_art_of_Star_Wars_Jedi_Fallen_Order.jpg"
        },
        {
            "id": 5,
            "title":"Borderlands 3",
            "text":"The original shooter-looter returns, packing bazillions of guns and a mayhem-fueled adventure! Blast through new worlds & enemies and save your home from the most ruthless cult leaders in the galaxy.",
            "image": "https://upload.wikimedia.org/wikipedia/en/2/21/Borderlands_3_cover_art.jpg"
        },
        {
            "id": 6,
            "title":"Assassin\'s Creed® Odyssey",
            "text":"Choose your fate in Assassin\'s Creed® Odyssey.",
            "image": "https://upload.wikimedia.org/wikipedia/en/9/99/ACOdysseyCoverArt.png"
        },
        {
            "id": 7,
            "title":"Untitled Goose Game",
            "text":"It\'s a lovely day in the village, and you are a horrible goose.",
            "image": "https://store.playstation.com/store/api/chihiro/00_09_000/container/US/en/999/UP3971-CUSA23079_00-HONKHONKHONKHONK/1577217870000/image?w=240&h=240&bg_color=000000&opacity=100&_version=00_09_000"
        },
        {
            "id": 8,
            "title":"Detroit: Become Human",
            "text":"Detroit 2038. Technology has evolved to a point where human like androids are everywhere. They speak, move and behave like human beings, but they are only machines serving humans.",
            "image": "https://upload.wikimedia.org/wikipedia/en/e/ee/Detroit_Become_Human.jpg"
        },
        {
            "id": 9,
            "title":"Fortnite",
            "text":"Drop in to a New World.",
            "image": "https://upload.wikimedia.org/wikipedia/commons/0/0e/FortniteLogo.svg"
        }
    ]}';
});

/*Route::get('/article/{id}', function($id) {
    
    // TODO make and use an Article model (or controller) instead

    $articles = [
        1 => '{
            "id": 1,
            "title":"Control",
            "text":"...",
            "image": ""
        }',
        2 => '{
            "id": 2,
            "title":"The Outer Worlds",
            "text":"...",
            "image": ""
        }',
    ];

    return ( isset($articles[$id]) ) ? $articles[$id] : 'Article not found';

})->where('id', '[0-9]+');;
*/