<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArticlesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testArticlesApi()
    {
        $response = $this->get('/api/articles');

        //$response->dump();

        $response->assertStatus(200)
            ->assertJsonCount(1, $key = null) // 'articles'
            ->assertJsonStructure([
                'articles' => [
                    ['id', 'title', 'text', 'image']
                ]
            ]);
    }
}
